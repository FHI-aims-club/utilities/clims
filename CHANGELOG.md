# CHANGELOG

Contributors: Florian Knoop, Volker Blum, Sebastian Kokott

## 0.7.3

* FIX: Resolve issue "clims-aimsplot --species_dos error"
* ENH:  Label band plot with SR/SOC and spin up/down
* Changed the handling of clims-aimsplot. It is now required to specify what you like to plot (e.g. with with `--band`)
* Make mace dependency optional

## 0.7.0

* NEW: Adding support for (pre-)relaxation of structures with mace-mp-0
* ENH: Enable phonon band structure plotting with phonopy.

## 0.6.0

* NEW: Adding support for [Elastic](https://github.com/jochym/Elastic). You can compute the elastic tensor and the equation of state with the calling the wrapper `clims-elastic`.
* ENH: Add citation infrastructure.


## 0.5.0

* NEW: Enable user-defined projection on groups of species and atoms in aims.plot for specie-projected DOS and Millken-projected bands. For all groups, an l-projection can be specified. Example:
    ```
    clims-aimsplot --species="Pb, I, H N C:p" --colors="purple, green, gold"
    ```
    This will plot the p-projected bands or species-projected DOS for the three groups. It is possible to define new colors for each group. 
* ENH: Simplify phonopy interface to `clims-phonopy`.  `initialize` and `postprocess` are now arguments of `clims-phonopy`.
* ENH: It is now possible to add multiple key-value pairs to the control.in file, e.g. `clims-prepare-run --keyword k_grid "4 4 4" --keyword charge -1`.
* ENH: Allow setting vdW flag in `clims-prepare-run`
* FIX: Resolve issue "Band path with 1 or 0 points".


## 0.4.7

* FIX: Resolve issue "Wrong path length in aimsplot.py".


## 0.4.6

* ENH: Add forces and stress to `clims-prepare-run`


## 0.4.5

* FIX: Using filename for control.in in calc.write_control(c, filename) calls. This makes ase 3.21 and 3.22 compatible.
* ENH: A little clean-up of the prepare_input.py file.
* ENH: Adding warning if no xc flag or no k_grid flag (for solids) is detected in an existing control.in file.


## 0.4.4

* ENH: Regression test improved when geometry.in file are read.
* ENH: Regression test added for phonopy interface.
* ENH:Help text for reinitialize_geometry routine.